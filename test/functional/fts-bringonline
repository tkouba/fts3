#!/usr/bin/env python
# meta: proxy=true
# meta: preconfig=../FTS3-config
import itertools
import logging
from lib import base, storage


class TestBringOnline(base.TestBase):
    
    def setUp(self):
        base.TestBase.setUp(self)
        # Remove non-srm surls
        self.transfers = filter(lambda pair: pair[0].startswith('srm://'), self.transfers)
        
    def test_bring_online(self):
        """
        Do a transfer enabling bring online
        """
        for (src, dst) in self.transfers:
            logging.info("Transfer with bring online %s => %s" % (src, dst))
            self.surl.create(src)
            jobId = self.client.submit([{'sources': [src], 'destinations': [dst]}], ['--bring-online=120'])
            logging.info("Got job id %s" % jobId)
            
            state = self.client.poll(jobId)
            
            self.assertEqual('FINISHED', state, 'job.state')
    
    def test_bring_online_only(self):
        """
        Source and destination are the same, so only bring online is done
        """
        for (src, dst) in self.transfers:
            logging.info("Transfer with bring online %s => %s" % (src, dst))
            self.surl.create(src)
            jobId = self.client.submit([{'sources': [src], 'destinations': [src]}], ['--bring-online=120'])
            logging.info("Got job id %s" % jobId)
            
            state = self.client.poll(jobId)
            
            self.assertEqual('FINISHED', state, 'job.state')

    def test_bringonline_multiple(self):
        """
        Regression test
        Submit a bring online job with a file that exist and another that doesn't
        """
        for (src, dst) in self.transfers:
            logging.info("Bring online only, finish dirty")
            self.surl.create(src)
            enoent = src + ".enoent"
            jobId = self.client.submit([
                {'sources': [enoent], 'destinations': [enoent]},
                {'sources': [src], 'destinations': [src]}
            ], ['--bring-online=120'])
            logging.info("Got job id %s" % jobId)

            state = self.client.poll(jobId)

            self.assertEqual('FINISHEDDIRTY', state, 'job.state')

            files = self.client.getFileInfo(jobId)
            self.assertEqual('FAILED', files[(enoent, enoent)]['state'], 'enoent.state')
            self.assertEqual('FINISHED', files[(src, src)]['state'], 'src.state')


if __name__ == '__main__':
    import sys
    sys.exit(TestBringOnline().run())
