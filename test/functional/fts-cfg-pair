#!/usr/bin/env python
# meta: proxy=true
# meta: preconfig=../FTS3-config
import itertools
import logging
import time
from lib import base, fts3, storage


class TestCfgPair(base.TestBase):
    
    def setUp(self):
        self.transfers = {}
        self.configuration = {}
        
        for (srcBaseSurl, dstBaseSurl) in storage.getStoragePairs():
            srcSE = storage.getStorageElement(srcBaseSurl)
            dstSE = storage.getStorageElement(dstBaseSurl)
            nestedTransfers = []
            for i in range(3):
                srcSurl = self.surl.generate(srcBaseSurl)
                dstSurl = self.surl.generate(dstBaseSurl)
                nestedTransfers.append((srcSurl, dstSurl))
            self.transfers[(srcSE, dstSE)] = nestedTransfers
            
            logging.info("Recovering current configuration for %s => %s" % (srcSE, dstSE))
            self.configuration[(srcSE, dstSE)] = {'before': self.client.getConfig(srcSE)}


    def resetConfig(self, pair):
        config = self.configuration[pair]
        before = config.get('before', None)
        after  = config.get('after', None)
        if before:
            logging.info("Restoring configuration for %s => %s" % pair)
            self.client.setConfig(before)
        elif after:
            logging.info("Removing configuration for %s => %s" % pair)
            self.client.delConfig(after)

        config['after'] = config['before']


    def tearDown(self):
        for (srcSE, dstSE) in self.transfers.keys():
            self._removeFiles(self.transfers[(srcSE, dstSE)])

        for pair in self.configuration.keys():
            self.resetConfig(pair)


    def transfer_and_poll(self, transfers):
        # Create the sources
        logging.info("Creating source files")
        for (src, dst) in transfers:
            self.surl.create(src)
        # Trigger three transfers
        logging.info("Spawning transfers")
        jobIds = []
        for (src, dst) in transfers:
            jobIds.append(self.client.submit([{'sources': [src], 'destinations': [dst]}]))
        # Query state and count actives
        logging.info("Entering polling...")
        submittedCount = len(transfers)
        activeCount = 0
        while (submittedCount + activeCount) > 0:
            states = map(lambda id: self.client.getJobState(id), jobIds)
            terminalCount = sum([1 for state in states if state in fts3.JobTerminalStates])
            activeCount = states.count('ACTIVE') + states.count('READY')
            submittedCount = states.count('SUBMITTED')
            
            totalCount = terminalCount + activeCount + submittedCount
            self.assertEqual(len(transfers), totalCount, 'jobs.count()')
            # Saldy, this can not be really asserted, since limits are soft
            #self.assertLessEqualThan(activeCount, 1)
            logging.debug("%d submitted, %d active, %d terminal" % (submittedCount, activeCount, terminalCount))

            time.sleep(1)
        return states


    def test_pair_simple(self):
        """
        Configure a pair with a non-zero inbound/outbound
        """
        for (srcSE, dstSE) in self.transfers:
            # Set up link config
            logging.info("Limiting inbound/outbound for %s => %s (%s) to 1" % (srcSE, dstSE, self.voName))
            newConfig = {
                'symbolic_name': 'test-pair-simple-link',
                'source_se': srcSE,
                'destination_se': dstSE,
                'active': True,
                'share': [{self.voName: 1}],
                'protocol': [{'urlcopy_tx_to': 'auto'}]
            }
            self.configuration[(srcSE, dstSE)]['after'] = newConfig
            self.client.setConfig(newConfig)
            
            # Transfer should pass
            transfers = self.transfers[(srcSE, dstSE)]
            states = self.transfer_and_poll(transfers)
            
            # They should have passed
            self.assertEqual(len(transfers), states.count('FINISHED'), 'jobs.count("FINISHED")')
            
            # Reset
            self.resetConfig((srcSE, dstSE))


    def test_pair_src_zero(self):
        """
        Configure a pair with zero inbound/outbound
        """
        for (srcSE, dstSE) in self.transfers:
            # Set up link config
            logging.info("Limiting inbound/outbound for %s => %s (%s) to 0" % (srcSE, dstSE, self.voName))
            newConfig = {
                'symbolic_name': 'test-pair-simple-link',
                'source_se': srcSE,
                'destination_se': dstSE,
                'active': True,
                'share': [{self.voName: 0}],
                'protocol': [{'urlcopy_tx_to': 'auto'}]
            }
            self.configuration[(srcSE, dstSE)]['after'] = newConfig
            self.client.setConfig(newConfig)
            
            # Transfer should pass
            transfers = self.transfers[(srcSE, dstSE)]
            states = self.transfer_and_poll(transfers)
            
            # They should have passed
            self.assertEqual(len(transfers), states.count('FAILED'), 'jobs.count("FAILED")')
            
            # Reset
            self.resetConfig((srcSE, dstSE))


if __name__ == '__main__':
    import sys
    sys.exit(TestCfgPair().run())
