#!/usr/bin/env python
# meta: proxy=true
# meta: preconfig=../FTS3-config
import itertools
import logging
from lib import base, storage 


class TestMultiple(base.TestRepeatEach):

    def setUp(self):
        # Need to create three sources and three destinations
        self.transfers = []
        for (srcSa, dstSa) in storage.getStoragePairs():
            pairs = []
            for i in range(3):
                s = self.surl.generate(srcSa)
                d = self.surl.generate(dstSa)
                pairs.append((s, d))
            self.transfers.append(pairs)
            
    def iterations(self):
        for t in self.transfers:
            t_repr = "%s => %s" % (t[0][0].split(':')[0], t[0][1].split(':')[0])
            yield (t_repr, t)

    def test_multiple(self, transfer):
        """
        Transfer multiple files without reusing
        """
        logging.info("Transfer multiple files without reuse")
        files = []
        for (src, dst) in transfer:
            logging.info("%s => %s" % (src, dst))
            self.surl.create(src)
            files.append({'sources': [src], 'destinations': [dst]})

        jobId = self.client.submit(files)
        logging.info("Got job id %s" % jobId)
        state = self.client.poll(jobId)
        logging.info("Job %s finished with %s" % (jobId, state))
        self.assertEqual('FINISHED', state, 'job.state')

        files = self.client.getFileInfo(jobId)
        i = 0
        for pair in files.keys():
            label = 'jobs.files[' + str(i) + '].state'
            self.assertEqual('FINISHED', files[pair]['file_state'], label)
            i += 1

    def test_multipleDirty(self, transfer):
        """
        Transfer multiple without reuse and one of them
        will fail, so the status should be FINISHEDDIRTY
        """
        logging.info("Transfer multiple files without reuse. One will fail")
        files = []
        nCreate = len(transfer) - 1
        created = []
        for (src, dst) in transfer:
            logging.info("%s => %s" % (src, dst))
            if nCreate:
                self.surl.create(src)
                created.append(src)
                nCreate -= 1
            else:
                logging.debug("Not creating %s" % src)
            files.append({'sources': [src], 'destinations': [dst]})

        jobId = self.client.submit(files)
        logging.info("Got job id %s" % jobId)
        state = self.client.poll(jobId)
        logging.info("Job %s finished with %s" % (jobId, state))
        self.assertEqual('FINISHEDDIRTY', state, 'job.state')

        files = self.client.getFileInfo(jobId)
        i = 0
        for (src, dst) in files.keys():
            label = 'jobs.files[' + str(i) + '].state'
            if src in created:
                self.assertEqual('FINISHED', files[(src, dst)]['file_state'], label)
            else:
                self.assertEqual('FAILED', files[(src, dst)]['file_state'], label)
            i += 1

if __name__ == '__main__':
    import sys
    sys.exit(TestMultiple().run())
