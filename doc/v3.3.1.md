Release Notes - FTS - Version fts 3.3.1
=======================================

## Bug
* [[FTS-241]](https://its.cern.ch/jira/browse/FTS-241) - Transfers executed twice when the node pool changes
* [[FTS-273]](https://its.cern.ch/jira/browse/FTS-273) - URL Copy sends two messages for the same file if signals are involved
